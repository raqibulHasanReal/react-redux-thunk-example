import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'
import rootReducer from '../reducers'

const inisialStore = {};
const middleware = [thunk];

const store = createStore(
    rootReducer, 
    inisialStore, 
    applyMiddleware(...middleware)
);

export default store;