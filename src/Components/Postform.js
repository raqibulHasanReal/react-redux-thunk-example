import React, { Component } from 'react'

class Postform extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title:'',
            body:'',
        }
    }

    onChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    onSubmit(event) {
        event.preventDefault();
        const post = {
            title: this.state.title,
            body: this.state.body
        }

        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(post)

        })
        .then(res => res.json())
        .then(data => console.log(data));
    }

    render() {
        return (
        <div className="col-sm-6 offset.sm-3">
            <h1>Add Post</h1>
            <form className="form-group" onSubmit = { (event)=>this.onSubmit(event) }>
                <div>
                    <label  htmlFor="title"> Title</label> <br/>
                    <input className="form-control" type="text" name="title" onChange= {(event) => this.onChange(event)} value={this.state.title} />
                </div>
                <br/>
                <div>
                    <label htmlFor="body"> Body</label> <br/>
                    <textarea className="form-control" name="body" id="" cols="50" rows="5" onChange= { (event)=> this.onChange(event)} value = {this.state.body} ></textarea>
                </div>
                <br/>
                <button className="btn btn-success" type="submit">Submit</button>

            </form>
        </div>
        )
    }
}

export default Postform;
